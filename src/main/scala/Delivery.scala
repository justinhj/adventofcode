package delivery

import java.io.InputStream
import scala.io.Source

// Day 3

object Delivery extends App {

  case class Coord(x: Long, y: Long)

  def incrementGiftCount(gifts: Map[Coord, Long], location: Coord): Map[Coord, Long] = {
    val giftCount: Long = gifts.getOrElse(location, 0)
    gifts + (location -> (giftCount + 1))
  }

  def delivery(stream: InputStream): Unit = {
    if(stream != null) {
      val r = Source.fromInputStream(stream).bufferedReader()
      val s = r.readLine()

      println(s"found ${s.size} line")

      // start off recording the gift at the current location
      var currentLocation: Coord = Coord(0,0)
      var giftsGiven = Map[Coord, Long](currentLocation -> 1)

      s.foreach{
        case '<' =>
          currentLocation = Coord(currentLocation.x - 1, currentLocation.y)
          giftsGiven = incrementGiftCount(giftsGiven, currentLocation)
        case '>' =>
          currentLocation = Coord(currentLocation.x + 1, currentLocation.y)
          giftsGiven = incrementGiftCount(giftsGiven, currentLocation)
        case '^' =>
          currentLocation = Coord(currentLocation.x, currentLocation.y - 1)
          giftsGiven = incrementGiftCount(giftsGiven, currentLocation)
        case 'v' =>
          currentLocation = Coord(currentLocation.x, currentLocation.y + 1)
          giftsGiven = incrementGiftCount(giftsGiven, currentLocation)
        case c: Char =>
          println(s"unhandled character $c")
      }



      println(s"houses deilvered to ${giftsGiven.size}")

    }
    else {
      throw new Exception("Couldn't locate resource")
    }
  }



  val stream: InputStream = getClass.getResourceAsStream("/input3.txt")

  delivery(stream)

  // TODO same thing but two deliverers that take it in turns to read from the instructions

  

}
