import java.io.InputStream
import lights.Lights._

import scala.io.Source

object Santa extends App {

  val stream: InputStream = getClass.getResourceAsStream("/input1a.txt")
  val lines = Source.fromInputStream(stream).getLines()

  var floor = 0

  //def fold[A1 >: A](z: A1)(op: (A1, A1) => A1): A1 = foldLeft(z)(op)

  while(lines.hasNext) {
    val s = lines.next

    floor = s.zipWithIndex.foldLeft(floor) {
      case (c: Int, (command: Char, pos: Int)) =>
        if(command == '(')
          c + 1
        else if(command == ')') {
          val newpos = c - 1
          if(newpos == -1) println(s"basement entered at $pos")
          newpos
        }

        else
          throw new Exception(s"unknown command $command")
    }
  }

  println(s"floor $floor")

}
