
object Elves extends App {


  def iter2(in: List[Int], cur: List[Int], acc: List[Int]): List[Int] = {

    //println(s"iter2 $in $cur $acc")

    in match {
      case head :: tail =>
        // no current so add head to current
        if(cur == Nil) iter2(tail, List(head), acc)
        // head is same as current head so add it
        else if(cur.head == head) iter2(tail, head :: cur, acc)
        // new number so add cur + head + count to the acc and clear current
        else iter2(tail, List(head), cur.head :: cur.size :: acc)

      case Nil =>
        // end of list no cur so we're done
        if(cur == Nil) acc
        // end of list neeed to handle adding current
        else cur.head :: cur.size :: acc
    }
  }

  def iter(in: List[Int], n: Int) : List[Int] = {
    if(n == 0) in
    else {
      val n1 = iter2(in, List(), List()).reverse
      //println(n1)
      iter(n1, n - 1)
    }
  }

  //iter(List(1), 5)
  val answer = iter(List(3,1,1,3,3,2,2,1,1,3), 50)

  println("answer len " + answer.size)


}
