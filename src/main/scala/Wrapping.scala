package wrapping

import java.io.InputStream
import scala.io.Source

object Wrapping extends App {

  case class Gift(l: Int, w: Int, h: Int) {

    def getRequiredRibbon() = {

      val shortest = List(l,w,h).sorted.take(2)

      2 * shortest(0) + 2 * shortest(1) + l * w * h
    }

    def getRequiredPaper() = {

      val shortest = List(l,w,h).sorted.take(2)

      2*l*w + 2*w*h + 2*h*l + shortest(1)*shortest(0)
    }
  }

  def parseLine(s: String): Gift = {
    val sides = s.split("x").map{_.toInt}
    Gift(sides(0), sides(1), sides(2))
  }

  def wrapping(stream: InputStream): Unit = {
    if(stream != null) {
      val lines = Source.fromInputStream(stream).getLines()
      //    println(s"found ${lines.size} lines")

      val g = Gift(2,3,4)
      val r = g.getRequiredPaper()

      println(s"2x3x4 $r")

      val g2 = Gift(1,1,10)
      val r2 = g2.getRequiredPaper()

      println(s"1x1x10 $r2")

      val gifts = lines.map{parseLine(_)}

      val total = gifts.foldLeft(0){
        (total, nextGift) =>
          total + nextGift.getRequiredPaper()
      }

      println(s"total paper required $total")

    }
    else {
      throw new Exception("Couldn't locate resource")
    }
  }

  def ribbon(stream: InputStream): Unit = {

    val g = Gift(2,3,4)
    val r = g.getRequiredRibbon()

    println(s"2x3x4 $r")

    val g1 = Gift(1,1,10)
    val r1 = g1.getRequiredRibbon()

    println(s"1x1x10 $r1")

    if(stream != null) {
      val lines = Source.fromInputStream(stream).getLines()

      val g2 = Gift(1,1,10)
      val r2 = g2.getRequiredPaper()

      val gifts = lines.map{parseLine(_)}

      val total = gifts.foldLeft(0){
        (total, nextGift) =>
          total + nextGift.getRequiredRibbon()
      }

      println(s"total ribbon required $total")

    }
    else {
      throw new Exception("Couldn't locate resource")
    }
  }

  val stream: InputStream = getClass.getResourceAsStream("/input2.txt")

  //wrapping(stream)
  ribbon(stream)

}
