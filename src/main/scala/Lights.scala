package lights

import java.io.InputStream

import scala.io.Source

/*
turn off 660,55 through 986,197
turn off 341,304 through 638,850
turn off 199,133 through 461,193
toggle 322,558 through 977,958
toggle 537,781 through 687,941
turn on 226,196 through 599,390
 */


object Lights extends App {

  sealed trait Command
  object Toggle extends Command
  object TurnOn extends Command
  object TurnOff extends Command

  //  val testCommands = Array(
  //    "turn off 660,55 through 986,197",
  //    "turn off 341,304 through 638,850",
  //    "turn off 199,133 through 461,193",
  //    "toggle 322,558 through 977,958",
  //    "toggle 537,781 through 687,941",
  //    "turn on 226,196 through 599,390"
  //  )

  //val l = new Lights(1000, 1000, testCommands.toIterator)

  val stream: InputStream = getClass.getResourceAsStream("/input6.txt")

  if(stream != null) {
    val lines = Source.fromInputStream(stream).getLines()
    new Lights(1000,1000,lines)
  }
  else {
    throw new Exception("Couldn't locate resource")
  }


}

class Lights(width: Int, height: Int, input: Iterator[String]) {

  import Lights.{Command, TurnOff, TurnOn, Toggle}

  var grid = Array.ofDim[Int](width, height)

  for(
    x <- 0 until width;
    y <- 0 until height)
    grid(x)(y) = 0

  def processCommand(s: String) : Unit = {
    var command : Option[Command] = None

    val turnOff = "turn off "
    val turnOn = "turn on "
    val toggle = "toggle "

    var rest = ""

    if(s.startsWith(turnOff)) {
      command = Some(TurnOff)
      rest = s.substring(turnOff.size)
    }

    else if(s.startsWith(turnOn)) {
      command = Some(TurnOn)
      rest = s.substring(turnOn.size)
    }

    else if(s.startsWith(toggle)) {
      command = Some(Toggle)
      rest = s.substring(toggle.size)
    }

    val parts = rest.split(' ')

    // splits into coords, through, coords

    val coords1 = parts(0).split(',').map(_.toInt)
    val coords2 = parts(2).split(',').map(_.toInt)

    //println(s"command $command coords1 ${coords1(0)} ${coords1(1)} coords2 ${coords2(0)} ${coords2(1)}")

    val startX = coords1(0)
    val endX = coords2(0)
    val startY = coords1(1)
    val endY = coords2(1)

    var y = startY;

    while(y <= endY) {

      var x = startX;

      while(x <= endX) {

        command match {
          case Some(TurnOn) => grid(x)(y) = grid(x)(y) + 1
          case Some(TurnOff)=> grid(x)(y) = Math.max(grid(x)(y) - 1, 0)
          case Some(Toggle) => grid(x)(y) = grid(x)(y) + 2
          case None =>
            println("No Command")
        }

        x += 1
      }

      y += 1
    }

  }

  while(input.hasNext) {
    val n = input.next
    processCommand(n)
  }

  var sum = 0

  for(
    x <- 0 until width;
    y <- 0 until height)
    sum += grid(x)(y)


  println(s"sum of lit $sum")

}


